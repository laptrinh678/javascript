// kiểm tra định dạng file
function check_type_file(datafile) {
    var file = ['png', 'gif', 'jpeg', 'jpg'];

    if (file.includes(datafile) == true) {
        return 1;
    } else {
        return '';
    }
}
// tìm kiếm và thay thế chuỗi 
// nguồn https://www.codegrepper.com/code-examples/javascript/replace+%27%2Fn%27+in+string+in+js%3F
function nl2br(str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
}

function replace_str(texttalk) {
    return texttalk.replace(/(\r\n|\n|\r)/gm, ",");
}

function replace3() {
    var r = "I\nam\nhere";
    return r.replace(/\n/g, ' ');
}
// các hàm xử lý trình duyệt web
//1 kiểm tra loại trình duyệt
function browserName() {
    var Browser = navigator.userAgent;
    if (Browser.indexOf('MSIE') >= 0) {
        Browser = 'MSIE';
    } else if (Browser.indexOf('Firefox') >= 0) {
        Browser = 'Firefox';
    } else if (Browser.indexOf('Chrome') >= 0) {
        Browser = 'Chrome';
    } else if (Browser.indexOf('Safari') >= 0) {
        Browser = 'Safari';
    } else if (Browser.indexOf('Opera') >= 0) {
        Browser = 'Opera';
    } else {
        Browser = 'UNKNOWN';
    }
    return Browser;
}

// Hàm random ký tự kết quả trả về là chuỗi ký tự random mỗi lần reload trình duyệt hoặc 1 sự kiện nào đó
function generateString(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = ' ';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result.toLowerCase();
}